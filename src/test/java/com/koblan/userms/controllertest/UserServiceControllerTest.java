package com.koblan.userms.controllertest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.service.RoleService;
import com.koblan.userms.service.UserService;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;

public class UserServiceControllerTest extends AbstractTest {
	
	
	   @Autowired
	   RoleService roleService;
	   @Autowired
	   RoleRepository roleRepository;
	   @Autowired
	   UserService userService;
	   @Autowired
	   UserRepository userRepository;
	   
	   @Before
	   public void setUp() {
		   super.setUp();
	   }
	   
	   @Before
	   public void open() {
	     //roleService.createRole(new Role("NoRole"));
	     //roleService.createRole(new Role("ADMIN"));
	     //roleService.createRole(new Role("Guest"));
	     //userService.createUser(new User("Olexiy","Artemenko","artem"));
	     //userService.createUser(new User("Oxana","Koval","oxakk22"));
	   }
	   
	   @After
	    public void close() {
		  //roleRepository.deleteAll();
		  //userRepository.deleteAll();
	    }
	   
	   @Test
	   public void getUsersList() throws Exception {
	      String uri = "/users";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      User[] userlist = super.mapFromJson(content, User[].class);
	      assertTrue("List of User retrived",userlist.length > 0);
	   }
	   
	   //@Ignore
	   @Test
	   public void createUser() throws Exception {
	      String uri = "/users";
	      User user = new User();
	      user.setFirstname("Ivan");
	      user.setLastname("Boyko");
	      user.setUsername("ivan22");
	      String inputJson = super.mapToJson(user);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      //assertEquals(content, "User is created successfully");
	      User user1=super.mapFromJson(content, User.class);
	      assertEquals("firstname equals",user.getFirstname(), user1.getFirstname());
	      assertEquals("lastname equals",user.getLastname(), user1.getLastname());
	      assertEquals("username equals",user.getUsername(), user1.getUsername());
	      
	   }
	   
	   //@Ignore
	   @Test
	   public void updateUser() throws Exception {
	      String uri = "/users/1";
	      User user = new User();
	      user.setFirstname("Igor");
	      user.setLastname("Artemenko");
	      user.setUsername("Artii");
	      Set<Role> roles=new HashSet<Role>();
	      Role r=new Role("ADMIN");
	      r.setId(roleRepository.findByRoleIgnoreCase("ADMIN").getId());
	      System.out.println(roleRepository.findByRoleIgnoreCase("ADMIN").getId());
	      roles.add(r);
	      user.setRoles(roles);
	      String inputJson = super.mapToJson(user);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      User user1=super.mapFromJson(content, User.class);
	      assertEquals("firstname equals",user.getFirstname(), user1.getFirstname());
	      assertEquals("lastname equals",user.getLastname(), user1.getLastname());
	      assertEquals("username equals",user.getUsername(), user1.getUsername());
	      assertEquals("user received admin role",user.getRoles().iterator().next().getRole().toString(), user1.getRoles().iterator().next().getRole().toString().toString());
	   }
	   
	   @Ignore
	   @Test
	   public void deleteUser() throws Exception {
	      String uri = "/users/2";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals("Deleted", 200, status);
	   }
}
