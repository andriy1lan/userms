
package com.koblan.userms.repository;

import com.koblan.userms.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	
	Role findByRoleIgnoreCase(String role);
}

