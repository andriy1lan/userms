package com.koblan.userms.service;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import javax.transaction.Transactional;
import com.koblan.userms.exceptions.*;

@Service
public class RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public List<Role> getAllRoles() {
		Comparator<Role> idComparator=Comparator.comparing(Role::getId);
		roleRepository.findAll().sort(idComparator);
        return roleRepository.findAll();
    }

    public Role getRole(Long id) throws NoSuchRoleException {
//      Role role = roleRepository.findOne(id);//1.5.9
        Role role = roleRepository.findById(id).get();//2.0.0.M7
        if (role == null) throw new NoSuchRoleException();
        return role;
    }
    
    @Transactional
    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    @Transactional
    public void updateRole(Role uRole, Long id) throws NoSuchRoleException {

        Role role = roleRepository.findById(id).get();
        if (role == null) throw new NoSuchRoleException();
        role.setRole(uRole.getRole());
        roleRepository.save(role);
    }
    
    @Transactional
    public void deleteRole(Long id) throws NoSuchRoleException, ExistsUsersForRoleException {

    	Role role = roleRepository.findById(id).get();
        if (role == null) throw new NoSuchRoleException();
        if (role.getUsers().size() != 0) throw new ExistsUsersForRoleException();
        roleRepository.delete(role);
    }
 
}