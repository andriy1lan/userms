package com.koblan.userms.service;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import com.koblan.userms.exceptions.*;

@Service
public class UserService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public List<User> getAllUsers() {
        return userRepository.findAll();
        
    }
	
	public User getUser(Long id) throws NoSuchUserException {
        User user = userRepository.findById(id).get();//2.0.0.M7
        if (user == null) throw new NoSuchUserException();
        return user;
    }
	
	@Transactional
    public User createUser(User user) {
		Role role=roleRepository.findByRoleIgnoreCase("Guest");
		Long id=3L; //ID of Guest
		//Role role=roleRepository.findById(id).get();
		Set<Role> r=new HashSet<Role>();
		r.add(role);
		user.setRoles(r);
		//user.getRoles().add(role);
		role.getUsers().add(user);
        return userRepository.save(user);
    }
	
	@Transactional
    public User createUserWithAdminRole(User user) {
		Role role=roleRepository.findByRoleIgnoreCase("Guest");
		Role role2=roleRepository.findByRoleIgnoreCase("Admin");
		Long id=3L; //ID of Guest
		//Role role=roleRepository.findById(id).get();
		Set<Role> r=new HashSet<Role>();
		r.add(role);
		//user.getRoles().add(role);
		role.getUsers().add(user);
		
		Long id2=2L; //ID of Admin
		//Role role2=roleRepository.findById(id2).get();
		r.add(role2);
		role2.getUsers().add(user);
		user.setRoles(r);
        return userRepository.save(user);
    }
	
	@Transactional
    public void updateUser(User nuser, Long id) throws NoSuchUserException {

        User user = userRepository.findById(id).get();
        
        if (user == null) throw new NoSuchUserException();
        user.setFirstname(nuser.getFirstname());
        user.setLastname(nuser.getLastname());
        user.setUsername(nuser.getUsername());
        user.setRoles(nuser.getRoles());
        userRepository.save(user);
    }
	
	    @Transactional
	    public void deleteUser(Long id) throws NoSuchUserException {
	    	User user = userRepository.findById(id).get();
	        if (user == null) throw new NoSuchUserException();
	        for(Role r:user.getRoles()) {r.getUsers().remove(user);}
	        userRepository.delete(user);
	    }
	
}
