package com.koblan.userms.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.koblan.userms.model.Role;

import com.koblan.userms.session.SessionFactoryCreator;

@Component
public class RoleDao implements Dao<Role>{

	public Role get (Long id) {
		return SessionFactoryCreator.getSession().get(Role.class,id);
	}

	public List<Role> getAll() {
		return (List<Role>)SessionFactoryCreator.getSession().createQuery( "from role").list();
	}
	
	public Role findByNameIgnoreCase (String name) {
		List<Role> allroles =(List<Role>)SessionFactoryCreator.getSession().createQuery( "from role").list();
		for (Role r:allroles) {if (r.getRole().equalsIgnoreCase(name)) return r;}
		return null;
	}

	public Role save(Role r) {
		Serializable id;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
		    tx = session.beginTransaction();
		    id=session.save(r);
		    
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		//    session.close();
		}
		   return session.get(Role.class,id); 
	}

	public Role update(Role r) {
		Role role;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
		    tx = session.beginTransaction();
		    role=(Role)session.merge(r);
		    
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		   session.close();
		}
		  return role;
	}

	public void delete(Role r) {
		Role role;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			role=(Role)session.merge(r);
			session.delete(role);
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		   session.close();
		}
		
	}

}
