package com.koblan.userms.dao;

import java.util.List;

public interface Dao<T> {
	     
	    T get(Long id);
	    List<T> getAll();
	    T save(T r);
	    T update(T r);
	    void delete(T r);

}
