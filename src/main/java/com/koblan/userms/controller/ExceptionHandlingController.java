package com.koblan.userms.controller;

import com.koblan.userms.model.Message;
import com.koblan.userms.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NoSuchRoleException.class)
    ResponseEntity<Message> showNoSuchRoleException(){
        return new ResponseEntity<Message>(new Message("Such role not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(NoSuchUserException.class)
    ResponseEntity<Message> showNoSuchUserException(){
        return new ResponseEntity<Message>(new Message("Such user not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(ExistsUsersForRoleException.class)
    ResponseEntity<Message> showExistsUsersForRoleException(){
        return new ResponseEntity<Message>(new Message("There are users for this role, cannot remove role"), HttpStatus.CONFLICT);
    }
}
